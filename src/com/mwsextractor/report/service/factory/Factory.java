package com.mwsextractor.report.service.factory;

import java.io.FileInputStream;
import java.util.Properties;

import javax.management.ServiceNotFoundException;

import com.mwsextractor.report.service.internal.IService;

//Factory pattern
public class Factory {
	//factory code to interface, return implementation
	private Factory(){} 
	
	private static Factory factory=new Factory(); //singleton design pattern
	
	public static Factory getInstance(){
		return factory;
	}
	
	public IService getService(String name) throws ServiceNotFoundException
	{
		try{
			
			Class c=Class.forName(getImplName(name));
			return(IService)c.newInstance();
			
		}catch (Exception e)
		{
			throw new ServiceNotFoundException();
		}
	}

	private String getImplName(String name) throws Exception {
		Properties props=new Properties();
		FileInputStream fis=new FileInputStream("config/config.txt");
		props.load(fis);
		fis.close();
		return props.getProperty(name);
	}
}
