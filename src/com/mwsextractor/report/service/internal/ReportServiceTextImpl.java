package com.mwsextractor.report.service.internal;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

public class ReportServiceTextImpl implements IReportService {

	public HashSet<String> GetNeededReports() throws   IOException {
		FileInputStream fis;
		try {
			fis = new FileInputStream("config/reportlist.txt");
		}

		catch (FileNotFoundException e) {
			throw new FileNotFoundException (e.getMessage());
		}

		
		BufferedReader br=new BufferedReader(new InputStreamReader(fis));
		String stringLine;
		HashSet<String> reportList=new HashSet<String>();
		
		//read file line-by-line
		try {
			while((stringLine=br.readLine())!=null)
			{
				reportList.add(stringLine);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new IOException (e.getMessage());
		}
		
		br.close();
		fis.close();
		return reportList;
	}
}
