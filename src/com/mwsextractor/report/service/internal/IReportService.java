package com.mwsextractor.report.service.internal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;

public interface IReportService extends IService{

	public HashSet<String> GetNeededReports() throws IOException ;

	
}
