/******************************************************************************* 
 *  Copyright 2009 Amazon Services.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 *
 *  Marketplace Web Service Java Library
 *  API Version: 2009-01-01
 *  Generated: Wed Feb 18 13:28:48 PST 2009 
 * 
 */



package com.mwsextractor.report.service.apicalls;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.amazonaws.mws.*;
import com.amazonaws.mws.model.*;
import com.mwsextractor.report.domain.RequestReportOutput;
 

/**
 *
 * Request Report  Samples
 *
 *
 */
public class SvcRequestReport {

    /**
     * Just add a few required parameters, and try the service
     * Request Report functionality
     *
     * @param args unused
     */
    public static RequestReportOutput  sendRequestReport (
			String accessKeyId
			, String secretAccessKey
			, String appName
			, String appVersion
			, String merchantId
			, String reportType
			
	)	throws MarketplaceWebServiceException {
        
        /************************************************************************
         * Access Key ID and Secret Access Key ID, obtained from:
         * http://aws.amazon.com
         ***********************************************************************/

        MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();

        /************************************************************************
         * Uncomment to set the appropriate MWS endpoint.
         ************************************************************************/
        // US
          config.setServiceURL("https://mws.amazonservices.com");
 
        /************************************************************************
         * You can also try advanced configuration options. Available options are:
         *
         *  - Signature Version
         *  - Proxy Host and Proxy Port
         *  - User Agent String to be sent to Marketplace Web Service
         *
         ***********************************************************************/

        /************************************************************************
         * Instantiate Http Client Implementation of Marketplace Web Service        
         ***********************************************************************/
        
        MarketplaceWebService service = new MarketplaceWebServiceClient(
                    accessKeyId, secretAccessKey, appName, appVersion, config);
 
 

        /************************************************************************
         * Marketplace and Merchant IDs are required parameters for all 
         * Marketplace Web Service calls.
         ***********************************************************************/
        RequestReportRequest request = new RequestReportRequest()
		        .withMerchant(merchantId)
		//        .withMarketplaceIdList(marketplaces)
		        .withReportType(reportType)
		       //.withReportOptions("ShowSalesChannel=true")
		        ;
        //request = request.withMWSAuthToken(sellerDevAuthToken);
        
        // demonstrates how to set the date range
		DatatypeFactory df = null;
		try {
			df = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		GregorianCalendar cal=new GregorianCalendar();
		
		if(reportType=="_GET_V1_SELLER_PERFORMANCE_REPORT_")
			cal.add(Calendar.DAY_OF_MONTH,-30);
		
		XMLGregorianCalendar startDate = df
				.newXMLGregorianCalendar(cal);
		request.setStartDate(startDate);
		
	    // @TODO: set additional request parameters here
		if(reportType=="_GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_" || reportType=="_GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_V2_")
		{	
			throw new MarketplaceWebServiceException("Settlement (_GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_ or _GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_V2_) \nCannot be requested through API");
		}
		else 
			return invokeRequestReport(service, request);
 
    }


                                                        
    /**
     * Request Report  request sample
     * requests the generation of a report
     *   
     * @param service instance of MarketplaceWebService service
     * @param request Action to invoke
     * @throws MarketplaceWebServiceException 
     */
    private static RequestReportOutput invokeRequestReport(MarketplaceWebService service, RequestReportRequest request) throws MarketplaceWebServiceException {
    	RequestReportOutput resp=new RequestReportOutput();
    	try {
            
            RequestReportResponse response = service.requestReport(request);
 
            if (response.isSetRequestReportResult()) {
 
                RequestReportResult  requestReportResult = response.getRequestReportResult();
                if (requestReportResult.isSetReportRequestInfo()) {
                    ReportRequestInfo  reportRequestInfo = requestReportResult.getReportRequestInfo();
                    if (reportRequestInfo.isSetReportRequestId()) {
                    	resp.setReportRequestId(reportRequestInfo.getReportRequestId());
                    }
                    if (reportRequestInfo.isSetReportType()) {
                    	resp.setReportType( reportRequestInfo.getReportType());
                    }
                    if (reportRequestInfo.isSetStartDate()) {
                    	resp.setStartDate(reportRequestInfo.getStartDate());
                    }
                    if (reportRequestInfo.isSetEndDate()) {
                    	resp.setEndDate(reportRequestInfo.getEndDate());
                    }
                    if (reportRequestInfo.isSetSubmittedDate()) {
                    	resp.setSubmittedDate(reportRequestInfo.getSubmittedDate());
                    }
                    if (reportRequestInfo.isSetReportProcessingStatus()) {

                    	resp.setReportProcessingStatus(reportRequestInfo.getReportProcessingStatus());
                    }
                } 
            } 
            if (response.isSetResponseMetadata()) {
                ResponseMetadata  responseMetadata = response.getResponseMetadata();
                if (responseMetadata.isSetRequestId()) {

                    resp.setRequestId( responseMetadata.getRequestId());
                }
            } 

           //return retVal;
            return resp;
        } catch (MarketplaceWebServiceException ex) {
            String exceptionMessage="";
            exceptionMessage+=("Caught Exception: " + ex.getMessage());
            exceptionMessage+=("Response Status Code: " + ex.getStatusCode());
            exceptionMessage+=("Error Code: " + ex.getErrorCode());
            exceptionMessage+=("Error Type: " + ex.getErrorType());
            exceptionMessage+=("Request ID: " + ex.getRequestId());
            exceptionMessage+=("XML: " + ex.getXML());
            exceptionMessage+=("ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata());
            throw new MarketplaceWebServiceException (exceptionMessage);
        }
 
    }
                                                
}
