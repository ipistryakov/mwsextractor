/******************************************************************************* 
 *  Copyright 2009 Amazon Services.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 *
 *  Marketplace Web Service Java Library
 *  API Version: 2009-01-01
 *  Generated: Wed Feb 18 13:28:48 PST 2009 
 * 
 */



package com.mwsextractor.report.service.apicalls;

import java.util.List;
import java.util.ArrayList;
import com.amazonaws.mws.*;
import com.amazonaws.mws.model.*;
import com.mwsextractor.report.domain.ReportIdType;
 

/**
 *
 * Get Report Request List  Samples
 *
 *
 */
public class SvcGetReportList {

    /**
     * Just add a few required parameters, and try the service
     * Get Report Request List functionality
     *
     * @param args unused
     * @throws MarketplaceWebServiceException 
     */
    public static  List<ReportIdType> GetReportList(String accessKeyId, String secretAccessKey, String appName, String appVersion, String merchantId) throws MarketplaceWebServiceException {
 
        MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
 
        // US
        config.setServiceURL("https://mws.amazonservices.com");
 
        /************************************************************************
         * You can also try advanced configuration options. Available options are:
         *
         *  - Signature Version
         *  - Proxy Host and Proxy Port
         *  - User Agent String to be sent to Marketplace Web Service
         *
         ***********************************************************************/

        /************************************************************************
         * Instantiate Http Client Implementation of Marketplace Web Service        
         ***********************************************************************/

        MarketplaceWebService service = new MarketplaceWebServiceClient(
                accessKeyId, secretAccessKey, appName, appVersion, config);
 
        GetReportListRequest request = new GetReportListRequest();
        request.setMerchant( merchantId );
        request.setAcknowledged(false);
        //request.setMWSAuthToken(sellerDevAuthToken);

        // @TODO: set request parameters here

         return invokeGetReportList(service, request);
    }



    /**
     * Get Report Request List  request sample
     * returns a list of report requests ids and their associated metadata
     *   
     * @param service instance of MarketplaceWebService service
     * @param request Action to invoke
     * @throws MarketplaceWebServiceException 
     */
    private static List<ReportIdType> invokeGetReportList(MarketplaceWebService service, GetReportListRequest request) throws MarketplaceWebServiceException {
        try {
        	
        	List<ReportIdType> reportList=new ArrayList<ReportIdType>();

            GetReportListResponse response = service.getReportList(request);

            if (response.isSetGetReportListResult()) {
                GetReportListResult  getReportListResult = response.getGetReportListResult();
                if (getReportListResult.isSetNextToken()) {
                    getReportListResult.getNextToken();

                }
                if (getReportListResult.isSetHasNext()) {
                    getReportListResult.isHasNext();

                }
                java.util.List<ReportInfo> reportInfoListList = getReportListResult.getReportInfoList();
                for (ReportInfo reportInfoList : reportInfoListList) {
                    ReportIdType report=new ReportIdType();
                    if (reportInfoList.isSetReportId()) {
                    	report.setReportId(reportInfoList.getReportId());
                        reportInfoList.getReportId();

                    }
                    if (reportInfoList.isSetReportType()) {
                    	report.setReportDataType(reportInfoList.getReportType());
                        reportInfoList.getReportType();
                    }
                    if (reportInfoList.isSetReportRequestId()) {
                        reportInfoList.getReportRequestId();
                    }
                    if (reportInfoList.isSetAvailableDate()) {
                        reportInfoList.getAvailableDate();
                    }
                    if (reportInfoList.isSetAcknowledged()) {
                        reportInfoList.isAcknowledged();
                    }
                    if (reportInfoList.isSetAcknowledgedDate()) {
                        reportInfoList.getAcknowledgedDate();
                    }
                    reportList.add(report);
                }
            } 
            if (response.isSetResponseMetadata()) {
                ResponseMetadata  responseMetadata = response.getResponseMetadata();
                if (responseMetadata.isSetRequestId()) {
                    responseMetadata.getRequestId();

                }
            } 

            response.getResponseHeaderMetadata();
            
            return reportList;

        } catch (MarketplaceWebServiceException ex) {
        	String exOut="";
        	exOut+="Caught Exception: " + ex.getMessage();
        	exOut+="Response Status Code: " + ex.getStatusCode();
        	exOut+="Error Code: " + ex.getErrorCode();
        	exOut+="Error Type: " + ex.getErrorType();
        	exOut+="Request ID: " + ex.getRequestId();
        	exOut+="XML: " + ex.getXML();
        	exOut+="ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata();
            throw new MarketplaceWebServiceException(exOut);
        }
		 
    }

}
