/******************************************************************************* 
 *  Copyright 2009 Amazon Services.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 *
 *  Marketplace Web Service Java Library
 *  API Version: 2009-01-01
 *  Generated: Wed Feb 18 13:28:48 PST 2009 
 * 
 */



package com.mwsextractor.report.service.apicalls;

import java.util.List;
import java.util.ArrayList;
import com.amazonaws.mws.*;
import com.amazonaws.mws.model.*;
 

/**
 *
 * Get Report Request List  Samples
 *
 *
 */
public class SvcGetReportRequestList {

    /**
     * Just add a few required parameters, and try the service
     * Get Report Request List functionality
     *
     * @param args unused
     * @throws MarketplaceWebServiceException 
     */
    public static List<ReportRequestInfo> GetReportRequestList(String accessKeyId, String secretAccessKey, String appName, String appVersion, String merchantId) throws MarketplaceWebServiceException {
    	List<ReportRequestInfo> reportRequestInfoList =new ArrayList<ReportRequestInfo>();
        MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();

        /************************************************************************
         * Uncomment to set the appropriate MWS endpoint.
         ************************************************************************/
        // US
        config.setServiceURL("https://mws.amazonservices.com");

        /************************************************************************
         * You can also try advanced configuration options. Available options are:
         *
         *  - Signature Version
         *  - Proxy Host and Proxy Port
         *  - User Agent String to be sent to Marketplace Web Service
         *
         ***********************************************************************/

        /************************************************************************
         * Instantiate Http Client Implementation of Marketplace Web Service        
         ***********************************************************************/

        MarketplaceWebService service = new MarketplaceWebServiceClient(
                accessKeyId, secretAccessKey, appName, appVersion, config);


        GetReportRequestListRequest request = new GetReportRequestListRequest();
        request.setMerchant( merchantId );
 
        //request.setMWSAuthToken(sellerDevAuthToken);

        // @TODO: set request parameters here

        reportRequestInfoList =invokeGetReportRequestList(service, request);
        return reportRequestInfoList;

    }



    /**
     * Get Report Request List  request sample
     * returns a list of report requests ids and their associated metadata
     *   
     * @param service instance of MarketplaceWebService service
     * @param request Action to invoke
     * @throws MarketplaceWebServiceException 
     */
    private static List<ReportRequestInfo> invokeGetReportRequestList(MarketplaceWebService service, GetReportRequestListRequest request) throws MarketplaceWebServiceException {
    	List<ReportRequestInfo> reportRequestInfoList =new ArrayList<ReportRequestInfo>();
    	try {

            GetReportRequestListResponse response = service.getReportRequestList(request);
            
            if (response.isSetGetReportRequestListResult()) {
                
                GetReportRequestListResult  getReportRequestListResult = response.getGetReportRequestListResult();
                if (getReportRequestListResult.isSetNextToken()) {
                    // getReportRequestListResult.getNextToken());
                }
                if (getReportRequestListResult.isSetHasNext()) {
                    getReportRequestListResult.isHasNext();
                }
                reportRequestInfoList = getReportRequestListResult.getReportRequestInfoList();
                for (ReportRequestInfo reportRequestInfo : reportRequestInfoList) {

                    if (reportRequestInfo.isSetReportRequestId()) {
                        reportRequestInfo.getReportRequestId();
                    }
                    if (reportRequestInfo.isSetReportType()) {
                        reportRequestInfo.getReportType();
                    }
                    if (reportRequestInfo.isSetStartDate()) {
                        reportRequestInfo.getStartDate( );
                    }
                    if (reportRequestInfo.isSetEndDate()) {
                        reportRequestInfo.getEndDate();
                        
                    }
                    if (reportRequestInfo.isSetSubmittedDate()) {
                    	reportRequestInfo.getSubmittedDate();
                    }
                    if (reportRequestInfo.isSetCompletedDate()) {
                        reportRequestInfo.getCompletedDate();
                    }                    
                    if (reportRequestInfo.isSetReportProcessingStatus()) {
                    	reportRequestInfo.getReportProcessingStatus();
                        
                    }
                    
                    return reportRequestInfoList;
                }
            } 
            if (response.isSetResponseMetadata()) {
                ResponseMetadata  responseMetadata = response.getResponseMetadata();
                if (responseMetadata.isSetRequestId()) {
                	responseMetadata.getRequestId();
                    
                }
            } 
             
            response.getResponseHeaderMetadata();
            


        } catch (MarketplaceWebServiceException ex) {
        	String outEx="";
            outEx+="Caught Exception: " + ex.getMessage();
            outEx+="Response Status Code: " + ex.getStatusCode();
            outEx+="Error Code: " + ex.getErrorCode();
            outEx+="Error Type: " + ex.getErrorType();
            outEx+="Request ID: " + ex.getRequestId();
            outEx+="XML: " + ex.getXML();
            outEx+="ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata();
            throw new MarketplaceWebServiceException (outEx);
        }
		return reportRequestInfoList;

		 
    }

}
