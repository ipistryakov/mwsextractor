/******************************************************************************* 
 *  Copyright 2009 Amazon Services.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 *
 *  Marketplace Web Service Java Library
 *  API Version: 2009-01-01
 *  Generated: Wed Feb 18 13:28:48 PST 2009 
 * 
 */



package com.mwsextractor.report.service.apicalls;

 
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import com.amazonaws.mws.*;
import com.amazonaws.mws.model.*;
import com.mwsextractor.report.util.Util;
 

/**
 *
 * Get Report  Samples
 *
 *
 */
public class SvcGetReport {

    /**
     * Just add a few required parameters, and try the service
     * Get Report functionality
     *
     * @param args unused
     * @throws FileNotFoundException 
     * @throws MarketplaceWebServiceException 
     */
    public static String GetReport
    	(
    			String accessKeyId
    			, String secretAccessKey
    			, String appName
    			, String appVersion
    			, String merchantId
    			, String reportId
    			, String reportType
    			
    	)		throws FileNotFoundException, MarketplaceWebServiceException {


        /************************************************************************
         * Access Key ID and Secret Access Key ID, obtained from:
         * http://aws.amazon.com
         ***********************************************************************/
     //   final String accessKeyId = "AKIAJ5KE7YNHMWKZLNVQ";
     //   final String secretAccessKey = "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";

//        final String appName = "BB_API_DEV";
  //      final String appVersion = ".000001a";

        MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();

        /************************************************************************
         * Uncomment to set the appropriate MWS endpoint.
         ************************************************************************/
        // US
        config.setServiceURL("https://mws.amazonservices.com");

        /************************************************************************
         * You can also try advanced configuration options. Available options are:
         *
         *  - Signature Version
         *  - Proxy Host and Proxy Port
         *  - User Agent String to be sent to Marketplace Web Service
         *
         ***********************************************************************/

        /************************************************************************
         * Instantiate Http Client Implementation of Marketplace Web Service        
         ***********************************************************************/

        MarketplaceWebService service = new MarketplaceWebServiceClient(
                accessKeyId, secretAccessKey, appName, appVersion, config);

        GetReportRequest request = new GetReportRequest();
        request.setMerchant( merchantId );
        //request.setMWSAuthToken(sellerDevAuthToken);
    //    String reportId="1173476610016828";
        request.setReportId( reportId );

        OutputStream report = new FileOutputStream( "C:\\Client\\BeachBody\\output\\" + reportId + reportType + Util.currentDateString() + ".txt" );
        request.setReportOutputStream( report );
        String retVal=invokeGetReport(service, request);
        SvcUpdateReportAcknowledgements.SetAcknowledged(accessKeyId, secretAccessKey, appName, appVersion, merchantId, reportId);
        return retVal;

    }



    /**
     * Get Report  request sample
     * The GetReport operation returns the contents of a report. Reports can potentially be
     * very large (>100MB) which is why we only return one report at a time, and in a
     * streaming fashion.
     *   
     * @param service instance of MarketplaceWebService service
     * @param request Action to invoke
     * @throws MarketplaceWebServiceException 
     */
    private static String invokeGetReport(MarketplaceWebService service, GetReportRequest request) throws MarketplaceWebServiceException {
    	String retVal="";
    	try {

            GetReportResponse response = service.getReport(request);
    		retVal+="MD5Checksum:  " + response.getGetReportResult().getMD5Checksum();
    		
            if (response.isSetResponseMetadata()) {
                ResponseMetadata  responseMetadata = response.getResponseMetadata();
                if (responseMetadata.isSetRequestId()) {
            		retVal+="\nRequestId:  " + responseMetadata.getRequestId();	
                }
            } 
            retVal+=request.getReportOutputStream().toString()  ;
            retVal+=response.getResponseHeaderMetadata();
 
            return retVal;

        } catch (MarketplaceWebServiceException ex) {
        	String exceptionString= 
        			"Caught Exception: " + ex.getMessage() 
        			+ "\nResponse Status Code: " + ex.getStatusCode()
        			+ "\nError Code: " + ex.getErrorCode()
        			+ "\nError Type: " + ex.getErrorType()
        			+ "\nRequest ID: " + ex.getRequestId()
        			+ "\nXML: " + ex.getXML()
        			+ "\nResponseHeaderMetadata: " + ex.getResponseHeaderMetadata();
        	
            throw new MarketplaceWebServiceException (exceptionString);
           
        }
    }

}
