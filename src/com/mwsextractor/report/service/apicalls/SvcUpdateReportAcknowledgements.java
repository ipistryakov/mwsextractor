/******************************************************************************* 
 *  Copyright 2009 Amazon Services.
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  
 *  You may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at: http://aws.amazon.com/apache2.0
 *  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
 *  CONDITIONS OF ANY KIND, either express or implied. See the License for the 
 *  specific language governing permissions and limitations under the License.
 * ***************************************************************************** 
 *
 *  Marketplace Web Service Java Library
 *  API Version: 2009-01-01
 *  Generated: Wed Feb 18 13:28:48 PST 2009 
 * 
 */



package com.mwsextractor.report.service.apicalls;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import com.amazonaws.mws.*;
import com.amazonaws.mws.model.*;
 

/**
 *
 * Update Report Acknowledgements  Samples
 *
 *
 */
public class SvcUpdateReportAcknowledgements  {

    /**
     * Just add a few required parameters, and try the service
     * Update Report Acknowledgements functionality
     *
     * @param args unused
     * @return 
     * @throws MarketplaceWebServiceException 
     */
    public static List<ReportInfo> SetAcknowledged(String accessKeyId, String secretAccessKey, String appName, String appVersion , String merchantId ,
    		String reportId
    		) throws MarketplaceWebServiceException {

 

        MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();

 
        // US
         config.setServiceURL("https://mws.amazonservices.com");
 

        /************************************************************************
         * You can also try advanced configuration options. Available options are:
         *
         *  - Signature Version
         *  - Proxy Host and Proxy Port
         *  - User Agent String to be sent to Marketplace Web Service
         *
         ***********************************************************************/

        /************************************************************************
         * Instantiate Http Client Implementation of Marketplace Web Service        
         ***********************************************************************/

        MarketplaceWebService service = new MarketplaceWebServiceClient(
                accessKeyId, secretAccessKey, appName, appVersion, config);


        /************************************************************************
         * Setup request parameters and uncomment invoke to try out 
         * sample for Update Report Acknowledgements 
         ***********************************************************************/

        /************************************************************************
         * Marketplace and Merchant IDs are required parameters for all 
         * Marketplace Web Service calls.
         ***********************************************************************/
 

        UpdateReportAcknowledgementsRequest request = new UpdateReportAcknowledgementsRequest();
        request.setMerchant( merchantId );
        IdList idList=new IdList();
        List<String> list=Arrays.asList(reportId);
        idList.setId(list);
         
        request.setReportIdList(idList);
        //request.setMWSAuthToken(sellerDevAuthToken);

        // @TODO: set request parameters here

        return invokeUpdateReportAcknowledgements(service, request);

    }



    /**
     * Update Report Acknowledgements  request sample
     * The UpdateReportAcknowledgements operation updates the acknowledged status of one or more reports.
     *   
     * @param service instance of MarketplaceWebService service
     * @param request Action to invoke
     * @throws MarketplaceWebServiceException 
     */
    private static List<ReportInfo>  invokeUpdateReportAcknowledgements(MarketplaceWebService service, UpdateReportAcknowledgementsRequest request) throws MarketplaceWebServiceException {
    	List<ReportInfo> reportInfoList = new ArrayList<ReportInfo>();
    	try {

    		UpdateReportAcknowledgementsResponse response  = service.updateReportAcknowledgements(request);

            if (response.isSetUpdateReportAcknowledgementsResult()) {
                UpdateReportAcknowledgementsResult  updateReportAcknowledgementsResult = response.getUpdateReportAcknowledgementsResult();
                if (updateReportAcknowledgementsResult.isSetCount()) {
                    updateReportAcknowledgementsResult.getCount();
                    
                }
                reportInfoList = updateReportAcknowledgementsResult.getReportInfoList();
                for (ReportInfo reportInfo : reportInfoList) {
                    if (reportInfo.isSetReportId()) {
                        reportInfo.getReportId();
                    }
                    if (reportInfo.isSetReportType()) {
                        reportInfo.getReportType();
                    }
                    if (reportInfo.isSetReportRequestId()) {
                        reportInfo.getReportRequestId();
                    }
                    if (reportInfo.isSetAvailableDate()) {
                        reportInfo.getAvailableDate();
                    }
                    if (reportInfo.isSetAcknowledged()) {
                        reportInfo.isAcknowledged();
                        
                    }
                    if (reportInfo.isSetAcknowledgedDate()) {
                       reportInfo.getAcknowledgedDate();
                        
                    }
                }
            } 
            if (response.isSetResponseMetadata()) {

                ResponseMetadata  responseMetadata = response.getResponseMetadata();
                if (responseMetadata.isSetRequestId()) {
                    responseMetadata.getRequestId();
                }
            } 
            response.getResponseHeaderMetadata();



        } catch (MarketplaceWebServiceException ex) {
        	String exOut="";
        	exOut+="Caught Exception: " + ex.getMessage();
        	exOut+="Response Status Code: " + ex.getStatusCode();
        	exOut+="Error Code: " + ex.getErrorCode();
        	exOut+="Error Type: " + ex.getErrorType();
        	exOut+="Request ID: " + ex.getRequestId();
        	exOut+="XML: " + ex.getXML();
        	exOut+="ResponseHeaderMetadata: " + ex.getResponseHeaderMetadata();
            throw new MarketplaceWebServiceException (exOut);
        }
		return  reportInfoList ;
    }

}
