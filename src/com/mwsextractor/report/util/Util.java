package com.mwsextractor.report.util;

 
import java.util.Calendar;

public class Util {
	public static String currentDateString()
	{
		Calendar cal=Calendar.getInstance();
		
		String dd="";
		if(cal.get(Calendar.DAY_OF_MONTH)<10)
			dd="0" + String.valueOf(cal.get(Calendar.DAY_OF_MONTH)) ;
		else
			dd=String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		
		String MM="";
		if(cal.get(Calendar.MONTH)+1<10)
			MM="0" + String.valueOf(cal.get(Calendar.MONTH)+1) ;
		else
			MM=String.valueOf(cal.get(Calendar.MONTH)+1);
		
		String yyyy=String.valueOf(cal.get(Calendar.YEAR));
		
		String HH="";
		if(cal.get(Calendar.HOUR)+1<10)
		{
			HH="0"+  String.valueOf(cal.get(Calendar.HOUR)+1);	
		}
		else		
			HH=String.valueOf(cal.get(Calendar.HOUR)+1);
		
		String mm="";
		if(cal.get(Calendar.MINUTE)+1<10)
		{
			mm="0"+  String.valueOf(cal.get(Calendar.MINUTE)+1);	
		}
		else		
			mm=String.valueOf(cal.get(Calendar.MINUTE)+1);
		
		
		String ss="";
		if(cal.get(Calendar.SECOND)+1<10)
		{
			ss="0"+  String.valueOf(cal.get(Calendar.SECOND)+1);	
		}
		else		
			ss=String.valueOf(cal.get(Calendar.SECOND)+1);	
		
		return yyyy+MM+dd+HH+mm+ss;
		
	}
}
