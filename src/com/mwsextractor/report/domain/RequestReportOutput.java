package com.mwsextractor.report.domain;

import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

public class RequestReportOutput {
	private String reportRequestId;//:  50020016829
	private String reportType;//:  _GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_
	private XMLGregorianCalendar startDate; //:  2011-02-01T08:00:00Z
	private XMLGregorianCalendar endDate; //:  2016-01-29T19:02:29Z
	private XMLGregorianCalendar submittedDate; //:  2016-01-29T19:02:29Z
	private String reportProcessingStatus; //_SUBMITTED_
	private String requestId; // :  781e8c6d-c780-4d7e-bea3-5e7a83b6eba5
	
	public RequestReportOutput(){//parameterless constructor
		
	}
	
	public RequestReportOutput(		//parametered constructor
			String reportRequestId, 
			String reportType, 
			XMLGregorianCalendar startDate, 
			XMLGregorianCalendar endDate,  
			XMLGregorianCalendar submittedDate,   
			String reportProcessingStatus,  
			String requestId 
		) 
	{
		this.reportRequestId= reportRequestId;  
		this.reportType= reportType; 
		this.startDate=startDate ; 
		this.endDate=endDate ; 
		this.submittedDate=submittedDate ; 
		this.reportProcessingStatus=reportProcessingStatus ; 
		this.requestId=requestId ; 
		
	}
	public void setReportRequestId(String reportRequestId){
		 this.reportRequestId= reportRequestId;
	} 
	public void setReportType(String reportType){
		this.reportType= reportType;
	}  
	public void setStartDate(XMLGregorianCalendar startDate){
		this.startDate= startDate;
	}  
	public void setEndDate(XMLGregorianCalendar endDate){
		this.endDate= endDate;
	}   
	public void setSubmittedDate(XMLGregorianCalendar submittedDate){
		this.submittedDate= submittedDate;
	}   
	public void setReportProcessingStatus(String reportProcessingStatus){
		this.reportProcessingStatus=  reportProcessingStatus;
	}  
	public void setRequestId(String requestId){
		this.requestId= requestId;
	} 
	
	
	
	public String getReportRequestId()
	{	
		return  reportRequestId;
	}
	
	public String getReportType()
	{
		return  reportType;
	}
	public XMLGregorianCalendar getStartDate()
	{
		return startDate;
	} 
	
	public XMLGregorianCalendar getEndDate()
	{
		return endDate;
	}  
	
	public XMLGregorianCalendar getSubmittedDate()  
	{
		return submittedDate;
	}
	
	public String getReportProcessingStatus()
	{
		return this.reportProcessingStatus;
	}
	
	public String getRequestId()
	{
		return this.requestId;
	}
	
	@Override
	public boolean equals(Object obj){
	    if (obj == null) {
	        return false;
	    }
	    if (this.getClass() != obj.getClass()) {
	        return false;
	    }
	    RequestReportOutput other = (RequestReportOutput) obj;
	    if ((this.reportRequestId == null) || (other.reportType != null) 
	    		|| this.reportProcessingStatus==null
	    		|| this.requestId==null)
	    {
	        return false;
	    }
	    
	    else if (this.hashCode()==other.hashCode())
	    		return true;
	    
	    else return false;
	}
	
	@Override
	public String toString(){
		String retVal="";
		retVal+="ReportRequestId:  " + getReportRequestId(); 
		retVal+="\nReportType:  " + getReportType();
		retVal+="\nStartDate:  "+ getStartDate();
    	retVal+="\nEndDate:  "+  getEndDate();
    	retVal+="\nSubmittedDate:  "+ getSubmittedDate();
        retVal+="\nReportProcessingStatus"+ getReportProcessingStatus();
        retVal+="\nRequestId:  " + getRequestId() ;
				
		return retVal;
	}

	@Override
	public  int hashCode(){
		return this.requestId.hashCode()+ this.reportRequestId.hashCode();
		
	}
	
}
