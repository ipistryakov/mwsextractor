package com.mwsextractor.report.domain;

public class ReportIdType {
	private String reportId;
	private String reportDataType;
	
	public String getReportId()
	{
		return this.reportId;
	}
	
	public void setReportId(String reportId)
	{
		this.reportId=reportId;	
	}
	
	public void setReportDataType(String reportDataType)
	{
		this.reportDataType=reportDataType;
	}
	
	public String getReportDataType()
	{
		return this.reportDataType;
	}

	@Override
	public String toString(){
		String retVal="";
		retVal+="Report Id:  " + getReportId(); 
		retVal+="\nReport Type:  " + getReportDataType();
				
		return retVal;
	}

}
